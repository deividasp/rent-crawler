package rentcrawler.xy.org.rent_crawler.impl;

import android.app.Service;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import rentcrawler.xy.org.rent_crawler.R;
import rentcrawler.xy.org.rent_crawler.util.NotificationUtils;

public class SkelbiuCrawler extends BaseCrawler {

    private static final String FILE_NAME = "skelbiu_links.json";
    private static final String CRAWL_URL = "https://www.skelbiu.lt/skelbimai/?keywords=&submit_bn=&cost_min=&cost_max=&space_min=&space_max=&rooms_min=&rooms_max=&year_min=&year_max=&building=0&status=0&floor_min=&floor_max=&floor_type=0&searchAddress=&district=0&quarter=0&streets=0&ignorestreets=0&cities=43&distance=0&mainCity=1&search=1&category_id=322&type=0&user_type=0&ad_since_min=0&ad_since_max=0&visited_page=1&orderBy=1&detailsSearch=1#logoAndMenu";

    public SkelbiuCrawler(Service service) {
        super(service);
    }

    @Override
    void notification(List<String> newLinks) {
        for (String link : newLinks) {
            NotificationUtils.notification(link, R.drawable.skelbiu_logo, "Skelbiu AD", "Tap the notification to open skelbiu.lt.", service);
        }
    }

    @Override
    List<String> processLinks(List<String> links) {
        List<String> newLinks = new ArrayList<>();

        for (String link : links) {
            newLinks.add("https://www.skelbiu.lt" + link);
        }

        return newLinks;
    }

    @Override
    String getFileName() {
        return FILE_NAME;
    }

    @Override
    String getCrawlURL() {
        return CRAWL_URL;
    }

    @Override
    String getSelectQuery() {
        return ".adsImage";
    }

}