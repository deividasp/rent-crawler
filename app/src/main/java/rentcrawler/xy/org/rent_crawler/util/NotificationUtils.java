package rentcrawler.xy.org.rent_crawler.util;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import rentcrawler.xy.org.rent_crawler.R;

public class NotificationUtils {

    private static int id = 0;

    public static void notification(String openUrl, int icon, String title, String text, Service service) {
        Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
        notificationIntent.setData(Uri.parse(openUrl));
        PendingIntent pi = PendingIntent.getActivity(service.getApplicationContext(), 0, notificationIntent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(service)
                        .setSound((Uri.parse("android.resource://" + service.getApplication().getPackageName() + "/" + R.raw.demonstrative)))
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pi);

        NotificationManager mNotificationManager =
                (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id++, mBuilder.build());
    }

}
