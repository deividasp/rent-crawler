package rentcrawler.xy.org.rent_crawler;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import rentcrawler.xy.org.rent_crawler.impl.AlioCrawler;
import rentcrawler.xy.org.rent_crawler.impl.AruodasCrawler;
import rentcrawler.xy.org.rent_crawler.impl.BaseCrawler;
import rentcrawler.xy.org.rent_crawler.impl.SkelbiuCrawler;

public class CrawlerService extends Service {

    private final IBinder binder = new Binder();

    public CrawlerService() {
    }

    @Override
    public void onCreate() {
        List<BaseCrawler> crawlers = new ArrayList<>();

        crawlers.add(new SkelbiuCrawler(this));
        crawlers.add(new AruodasCrawler(this));
        crawlers.add(new AlioCrawler(this));

        for (BaseCrawler crawler : crawlers) {
            crawler.start();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

}
