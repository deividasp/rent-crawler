package rentcrawler.xy.org.rent_crawler.impl;

import android.app.Service;
import android.content.Context;

import java.util.List;

import rentcrawler.xy.org.rent_crawler.R;
import rentcrawler.xy.org.rent_crawler.util.NotificationUtils;

public class AruodasCrawler extends BaseCrawler {

    private static final String FILE_NAME = "aruodas_links.json";
    private static final String CRAWL_URL = "https://www.aruodas.lt/butu-nuoma/kaune/?detailed_search=0&redirect_skelbiu=0&redirect_edomus=0&obj=4&FRegion=43&FDistrict=6&FAreaOverAllMax=&FAreaOverAllMin=&FRoomNumMax=&FRoomNumMin=&FFloorNumMax=&FFloorNumMin=&FFloor=0&FPriceMax=&FPriceMin=&FOrder=Actuality";

    public AruodasCrawler(Service service) {
        super(service);
    }

    @Override
    void notification(List<String> newLinks) {
        for (String link : newLinks) {
            NotificationUtils.notification(link, R.drawable.aruodas_logo, "Aruodas AD", "Tap the notification to open aruodas.lt.", service);
        }
    }

    @Override
    String getFileName() {
        return FILE_NAME;
    }

    @Override
    String getCrawlURL() {
        return CRAWL_URL;
    }

    @Override
    String getSelectQuery() {
        return ".list-photo > a";
    }

}