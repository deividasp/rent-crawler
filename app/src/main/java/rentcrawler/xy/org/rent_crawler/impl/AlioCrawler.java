package rentcrawler.xy.org.rent_crawler.impl;

import android.app.Service;
import android.content.Context;
import android.util.Log;

import java.util.List;

import rentcrawler.xy.org.rent_crawler.R;
import rentcrawler.xy.org.rent_crawler.util.NotificationUtils;

public class AlioCrawler extends BaseCrawler {

    private static final String FILE_NAME = "alio_links.json";
    private static final String CRAWL_URL = "http://www.alio.lt/paieska.html?category_id=1393&searchHash=a0567f5e7c76cde7a974c5e79e63f8b52853e5c0&order=A.ad_id%7CDESC";

    public AlioCrawler(Service service) {
        super(service);
    }

    @Override
    void notification(List<String> newLinks) {
        for (String link : newLinks) {
            NotificationUtils.notification(link, R.drawable.alio_logo, "Alio AD", "Tap the notification to open alio.lt.", service);
        }
    }

    @Override
    String getFileName() {
        return FILE_NAME;
    }

    @Override
    String getCrawlURL() {
        return CRAWL_URL;
    }

    @Override
    String getSelectQuery() {
        return ".image > a";
    }

}