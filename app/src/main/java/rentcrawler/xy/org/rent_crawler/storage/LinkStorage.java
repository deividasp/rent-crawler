package rentcrawler.xy.org.rent_crawler.storage;

import java.util.ArrayList;
import java.util.List;

public class LinkStorage {

    private List<String> links;

    public LinkStorage() {
        links = new ArrayList<>();
    }

    public LinkStorage(List<String> links) {
        this.links = links;
    }

    public List<String> getLinks() {
        return links;
    }

}