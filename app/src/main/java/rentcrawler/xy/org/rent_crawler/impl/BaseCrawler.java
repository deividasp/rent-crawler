package rentcrawler.xy.org.rent_crawler.impl;

import android.app.Service;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import rentcrawler.xy.org.rent_crawler.storage.LinkStorage;

public abstract class BaseCrawler {

    /*
     * Crawling rate in seconds
     */
    private static final int CRAWLING_RATE = 60;

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public final Service service;

    BaseCrawler(Service service) {
        this.service = service;

        File file = new File(Environment.getExternalStorageDirectory(), getFileName());

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Error"+Thread.currentThread().getStackTrace()[2],paramThrowable.getLocalizedMessage());
            }
        });

        if (!file.exists()) {
            try {
                file.createNewFile();

                Writer writer = new FileWriter(file);

                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                gson.toJson(new LinkStorage(), writer);

                writer.flush();
                writer.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void start() {
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    Document document = null;
                    try {
                         document = Jsoup.connect(getCrawlURL()).get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (document == null) {
                        return;
                    }
                    Elements linkElements = document.select(getSelectQuery());

                    List<String> links = new ArrayList<>();

                    for (Element element : linkElements) {
                        links.add(element.attr("href"));
                    }

                    links = processLinks(links);

                    List<String> savedLinks = getSavedLinks();
                    List<String> newLinks = new ArrayList<>();

                    for (String link : links) {
                        if (!savedLinks.contains(link)) {
                            newLinks.add(link);
                        }
                    }

                    if (!newLinks.isEmpty()) {
                        notification(newLinks);
                    }

                    List<String> newStorageLinks = new ArrayList<>();
                    newStorageLinks.addAll(savedLinks);
                    newStorageLinks.addAll(newLinks);

                    saveStorage(new LinkStorage(newStorageLinks));
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }, 0, CRAWLING_RATE, TimeUnit.SECONDS);
    }

    private List<String> getSavedLinks() throws IOException {
        try (Reader reader = new FileReader(new File(Environment.getExternalStorageDirectory(), getFileName()))) {
            Gson gson = new GsonBuilder().create();
            LinkStorage storage = gson.fromJson(reader, LinkStorage.class);
            return storage.getLinks();
        }
    }

    private void saveStorage(LinkStorage storage) throws IOException {
        try (Writer writer = new FileWriter(new File(Environment.getExternalStorageDirectory(), getFileName()))) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(storage, writer);
        }
    }

    List<String> processLinks(List<String> links) {
        return links;
    }

    abstract void notification(List<String> newLinks);

    abstract String getFileName();
    abstract String getCrawlURL();
    abstract String getSelectQuery();

}